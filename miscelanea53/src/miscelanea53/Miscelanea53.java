/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea53;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea53 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);

        int t[] = new int[10];
        int elemento, posicion;
       
        System.out.println("Leyendo datos...");
        
        for (int i = 0; i < 8; i++) {
            System.out.print("Introduzca número: ");
            t[i] = ab.nextInt();
        }
        
    
        System.out.print("Nuevo elemento: ");
        elemento = ab.nextInt();
        
        System.out.print("Posición donde insertar (de 0 a 8): ");
        posicion = ab.nextInt();
        
        for (int i = 7; i >= posicion; i--) {
            t[i + 1] = t[i];
        }
        
        t[posicion] = elemento;
        
        System.out.println("La tabla queda:");
        
        for (int i = 0; i < 9; i++) {
            System.out.println(t[i]);
        }

    }

}
