/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea44;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea44 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);

        int num;
        boolean multiplo_3;
        multiplo_3 = false;
        for (int i = 0; i < 5; i++) {
            System.out.print("Introduzca número: ");
            num = ab.nextInt();
            if (num % 3 == 0) {
                multiplo_3 = true;
            }
        }
        if (multiplo_3 == false) {
            System.out.println("no existen múltiplos de 3");
        } else {
            System.out.println("Hay múltiplos de 3");
        }

    }

}
