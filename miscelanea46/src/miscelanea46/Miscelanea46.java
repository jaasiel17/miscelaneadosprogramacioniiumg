/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea46;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea46 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);
        
        int n; 
        int fila, col;
        System.out.print("Lado del cuadrado: ");
        n = ab.nextInt();
        for (fila = 1; fila <= n; fila++) {
            for (col = 1; col <= n; col++) {
                System.out.print("* ");
            }
            System.out.println("");
        }

    }
    
}
