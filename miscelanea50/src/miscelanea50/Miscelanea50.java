/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea50;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea50 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);

        int t[] = new int[5];

        for (int i = 0; i < 5; i++) {
            System.out.print("Introduzca un número: ");
            t[i] = ab.nextInt();
        }

        System.out.println("Los números (en orden inverso):");
        for (int i = 4; i >= 0; i--) {
            System.out.println(t[i]);
        }

    }

}
