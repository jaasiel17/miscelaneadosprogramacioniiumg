/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea51;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea51 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner ab = new Scanner(System.in);
        
        int a[], b[], c[];
        int i, j;
        a = new int[10];
        b = new int[10];

        c = new int[20];

        System.out.println("Leyendo la tabla a");
        for (i = 0; i < 10; i++) {
            System.out.print("número: ");
            a[i] = ab.nextInt();
        }

        System.out.println("Leyendo la tabla b");
        for (i = 0; i < 10; i++) {
            System.out.print("número: ");
            b[i] = ab.nextInt();
        }

        j = 0;
        for (i = 0; i < 10; i++) {
            c[j] = a[i];
            j++;
            c[j] = b[i];
            j++;
        }
        System.out.println("La tabla c queda: ");
        for (j = 0; j < 20; j++) {
            System.out.print(c[j] + " ");
        }

        System.out.println("");

    }

}
