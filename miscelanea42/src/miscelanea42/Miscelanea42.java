/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea42;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea42 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner ab = new Scanner(System.in);

        int num;
        boolean hay_negativo;

        hay_negativo = false;

        for (int i = 1; i <= 10; i++) {
            System.out.print("Introduce número: ");
            num = ab.nextInt();
            
            if (num < 0) {
                hay_negativo = true;
            }

        }
        if (hay_negativo == true) {
            System.out.println("Se ha introducido algún número negativo");
        } else {
            System.out.println("No hay ningún número negativo");
        }
    }

}
