/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea52;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea52 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner ab = new Scanner(System.in);

        int numeros[];
        int i;
        boolean creciente, decreciente;

        numeros = new int[10];
        creciente = false;
        decreciente = false;

        System.out.println("Leyendo números:");

        for (i = 0; i < 10; i++) {
            System.out.print("número: ");
            numeros[i] = ab.nextInt();
        }

        for (i = 0; i < 9; i++) {
            if (numeros[i] > numeros[i + 1]) {
                decreciente = true;
            }
            if (numeros[i] < numeros[i + 1]) {
                creciente = true;
            }
        }

        if (creciente == true && decreciente == false) {
            System.out.println("Serie creciente.");
        }
        if (creciente == false && decreciente == true) {
            System.out.println("Serie decreciente.");
        }
        if (creciente == true && decreciente == true) {
            System.out.println("Serie desordenada.");
        }
        if (creciente == false && decreciente == false) {
            System.out.println("Todos los números iguales.");
        }
    }

}
