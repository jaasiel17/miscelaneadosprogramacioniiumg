/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea41;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea41 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner ab = new Scanner(System.in);

        int sueldo, sueldo_max = 0, n;

        System.out.print("Número de sueldos: ");
        n = ab.nextInt();
        System.out.println("--------");
        for (int i = 1; i <= n; i++) {
            System.out.print("Introduce sueldo: ");
            sueldo = ab.nextInt();
            if (sueldo > sueldo_max) {
                sueldo_max = sueldo;
            }

        }
        System.out.println("\nEl sueldo máximo es: " + sueldo_max);

    }

}
