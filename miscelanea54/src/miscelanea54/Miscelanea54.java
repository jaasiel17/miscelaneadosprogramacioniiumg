/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea54;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea54 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);

        int t[] = new int[10];
        int posicion;

        for (int i = 0; i < 10; i++) {
            System.out.print("Elemento (" + i + "): ");
            t[i] = ab.nextInt();
        }
        System.out.println();

        System.out.print("Posición a eliminar: ");
        posicion = ab.nextInt();

        for (int i = posicion; i < 9; i++) {
            t[i] = t[i + 1];
        }
        System.out.println("La tabla queda: ");
        for (int i = 0; i < 9; i++) {
            System.out.println(t[i]);
        }

    }

}
