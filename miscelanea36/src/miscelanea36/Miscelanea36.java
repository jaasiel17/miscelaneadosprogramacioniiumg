/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea36;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea36 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);
        double altura, edad, edadme, altume, mayordieci = 0, mayorunoset = 0, sumaal=0, sumaed=0;
        
        System.out.println("Ingresa la edad y estatura de 5 alumnos");
        
        for(int i = 0; i < 5; i++){
            System.out.print("Ingresa edad de alumno "+(i+1)+": ");
            edad = ab.nextDouble();
            System.out.println("Ingreda altura de alumno "+(i+1)+": ");
            altura = ab.nextDouble();
            sumaal = sumaal + altura;
            sumaed = sumaed + edad;
            
            if(edad>18){
                mayordieci++;
            }
            
            if(altura>1.75){
                mayorunoset++;
            }
           
        }
        
        edadme = sumaed/5;
        altume = sumaal/5;
        
        System.out.println("La edad media es: "+edadme);
        System.out.println("La altura media es: "+altume);
        System.out.println("La cantidad de estudiantes mayores de 18 son: "+mayordieci);
        System.out.println("La cantidad de estudiantes mayores de 1.75 metros son: "+mayorunoset);
        
    }
    
}
