/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea33;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea33 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner a = new Scanner(System.in);
        int num;
        double factorial = 1;
       
        System.out.print("Ingresa un numero y te dare su factorial: ");
        num = a.nextInt();
        
        for(int i=num; i>0; i--){
            factorial = factorial * i;
        }
        
        System.out.println("El factorial de "+num+" es: "+factorial);
    }
    
}
