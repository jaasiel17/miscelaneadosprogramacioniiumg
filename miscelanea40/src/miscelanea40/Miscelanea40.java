/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea40;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea40 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner ab = new Scanner(System.in);

        int nota, aprobados, suspensos, condicionados;
        aprobados = 0;
        suspensos = 0;

        condicionados = 0;

        for (int i = 1; i <= 6; i++) {
            System.out.print("Introduzca nota entre 0 y 10: ");
            nota = ab.nextInt();
            if (nota == 4) {
                condicionados++;
            } else if (nota >= 5) {
                aprobados++;
            } else if (nota < 4) {
                suspensos++;
            }

        }
        System.out.println("Aprobados: " + aprobados);
        System.out.println("Suspensos: " + suspensos);
        System.out.println("Condicionados: " + condicionados);

    }

}
