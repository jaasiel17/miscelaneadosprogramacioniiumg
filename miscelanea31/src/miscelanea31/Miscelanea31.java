/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea31;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea31 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);
        System.out.println("Dame 15 numeros y te daré la suma de ellos");
        int suma=0, num=0;
        
        for(int i = 0; i < 15; i++){
            System.out.print("Dame numero "+(i+1)+": ");
            num = ab.nextInt();
            suma += num;
        }
        
        System.out.println("La suma de los numeros es: "+suma);
        
    }
    
}
