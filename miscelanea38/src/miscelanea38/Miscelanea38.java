/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea38;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea38 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);
        int codigo, litros, litros_cod1 = 0, mas_600 = 0; 
        float precio, importe_factura, facturacion_total = 0; 
        
        
        for (int i=1;i<=5;i++)
        {
            
            System.out.println("Factura nº " + i);
            System.out.print("código de producto: ");
            codigo= ab.nextInt();
            System.out.print("cantidad (litros): ");
            litros= ab.nextInt();
            System.out.print("precio (litro): ");
            precio= ab.nextFloat();
            
            importe_factura = litros*precio;
            facturacion_total += importe_factura;
            
            if (codigo == 1)
            litros_cod1 += litros;
            
            if(importe_factura >= 600)
            mas_600 ++;
            
            }
        
            System.out.println("\n\n\nResumen de ventas\n");
            // facturación total
            System.out.println("La facturación total es de: " +facturacion_total + "€");

            // litros del articulo 1
             System.out.println("Ventas del producto 1: " + litros_cod1 + " litros");


            // factura de mas de 600 euros
            System.out.println("Factura superior a 600€: " + mas_600);

    }
    
}
