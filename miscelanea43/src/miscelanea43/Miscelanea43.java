/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea43;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea43 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner ab = new Scanner(System.in);

        int notas;
        boolean suspensos = false;

        for (int i = 0; i < 5; i++) {
            System.out.print("Introduzca nota (de 0 a 10): ");
            notas = ab.nextInt();
            if (notas < 5) {
                suspensos = true;
            }
        }
        if (suspensos) {
            System.out.println("Hay alumnos suspensos");
        } else {
            System.out.println("No hay suspensos");
        }
    }

}
