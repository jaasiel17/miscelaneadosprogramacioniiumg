/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea37;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea37 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);
        
        int num;
            do
            {
                System.out.print("Introduce número (de 0 a 10): ");
                num = ab.nextInt();
            }while(! (0<=num && num<=10));
            
            System.out.println("\n\nTabla del " + num);

            for (int i=1;i<=10;i++){
                
            System.out.println(num + " x " + i + " = " + num*i);

            }
        }
    
    }
