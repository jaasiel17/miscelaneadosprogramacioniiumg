/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea34;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea34 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner ab = new Scanner(System.in);
        double num, contp=0, contn=0, contc=0, suma = 0, suman=0;
        
        System.out.println("Dame 10 numeros");
        
        for(int i = 0; i < 10; i++){
            System.out.print("Dame numero "+(i+1)+": ");
            num = ab.nextInt();
            
            if(num>0){
                contp++;
                suma = suma+num;
            }else if(num<0){
                contn++;
                suman = suman + num;
            }else if(num==0){
                contc++;
            }
        }
        
        System.out.println("Media de numeros positivos: "+(suma/contp));
        System.out.println("Media de numeros negativos: "+ (suman/contn));
        System.out.println("Cantidad de ceros: "+contc);
    }
    
}
