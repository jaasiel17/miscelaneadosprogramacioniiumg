/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea48;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea48 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);

        int j, num, cont_pri=0;
        boolean primo;
       
        System.out.print("Introduce numero: ");
        num = ab.nextInt();
        
        for (int i = 1; i <= num; i++) {

            primo = true;
            j = 2;
            while (j <= i - 1 && primo == true) {
                if (i % j == 0) {
                    primo = false;
                }
                j++;
            }
            if (primo == true) {
                cont_pri++;
                System.out.println(i + (" es primo"));
            }
        }
        System.out.println("En el rango 1.." + num + ", hay " + cont_pri + " números primos");

    }

}
