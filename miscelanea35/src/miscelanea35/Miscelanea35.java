/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miscelanea35;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class Miscelanea35 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner ab = new Scanner(System.in);
        
        int num, suma=0, contmil=0;
        
        System.out.println("Dame 10 sueldos");
        for(int i = 0; i <10; i++){
            System.out.print("Dame sueldo No. "+(i+1)+": ");
            num = ab.nextInt();
            suma = suma +num;
            
            if(num>1000){
                contmil++;
            }
        }
        
        System.out.println("La suma es: "+suma);
        System.out.println("La cantidad de sueldos mayores a mil es: "+contmil);
        
    }
    
}
